package me.yanglw.android.roundlayout;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * 使用 {@link PorterDuffXfermode} 实现圆角 Layout 。
 *
 * Created by yanglw on 2016-05-06.
 */
public class RoundRectFrameLayout extends FrameLayout {
    private Path mPath = new Path();

    private Paint mClipPaint;

    private Paint mStrokePaint;
    private float mStrokeWidth;
    private RectF mOvalRectF = new RectF();

    private float mTopLeftRadius;
    private float mTopRightRadius;
    private float mBottomLeftRadius;
    private float mBottomRightRadius;

    public RoundRectFrameLayout(Context context) {
        this(context, null);
    }

    public RoundRectFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundRectFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RoundRectFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray a = context.obtainStyledAttributes(attrs,
                                                      R.styleable.RoundRectFrameLayout,
                                                      defStyleAttr,
                                                      defStyleRes);
        float radius = a.getDimension(R.styleable.RoundRectFrameLayout_roundRadius, 0);
        mTopLeftRadius = a.getDimension(R.styleable.RoundRectFrameLayout_roundTopLeftRadius, radius);
        mTopRightRadius = a.getDimension(R.styleable.RoundRectFrameLayout_roundTopRightRadius, radius);
        mBottomLeftRadius = a.getDimension(R.styleable.RoundRectFrameLayout_roundBottomLeftRadius, radius);
        mBottomRightRadius = a.getDimension(R.styleable.RoundRectFrameLayout_roundBottomRightRadius, radius);

        mClipPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        mClipPaint.setStyle(Paint.Style.FILL);
        mClipPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));

        if (a.hasValue(R.styleable.RoundRectFrameLayout_roundStrokeWidth)) {
            mStrokeWidth = a.getDimension(R.styleable.RoundRectFrameLayout_roundStrokeWidth, 0);
            int strokeColor = a.getColor(R.styleable.RoundRectFrameLayout_roundStrokeColor, 0);

            if (mStrokeWidth > 0) {
                mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
                mStrokePaint.setStyle(Paint.Style.STROKE);
                mStrokePaint.setStrokeWidth(mStrokeWidth);
                mStrokePaint.setColor(strokeColor);
            }
        }

        a.recycle();
        setWillNotDraw(false);
    }

    @Override
    public void draw(Canvas canvas) {
        int count = canvas.saveLayer(0, 0, getWidth(), getHeight(),
                                     isInEditMode() ? new Paint() : null,
                                     Canvas.ALL_SAVE_FLAG);

        super.draw(canvas);

        drawRoundRect(canvas);
        drawStroke(canvas);

        canvas.restoreToCount(count);
    }

    private void drawRoundRect(Canvas canvas) {
        int height = getHeight();
        int width = getWidth();

        // top-left
        clipCorner(canvas, mTopLeftRadius,
                   mTopLeftRadius, 0,
                   0, 0,
                   0, mTopLeftRadius,
                   0, 0,
                   180, 90);
        // top-right
        clipCorner(canvas, mTopRightRadius,
                   width, mTopRightRadius,
                   width, 0,
                   width - mTopRightRadius, 0,
                   width - mTopRightRadius * 2, 0,
                   270, 90);
        // bottom-right
        clipCorner(canvas, mBottomRightRadius,
                   width - mBottomRightRadius, height,
                   width, height,
                   width, height - mBottomRightRadius,
                   width - mBottomRightRadius * 2, height - mBottomRightRadius * 2,
                   0, 90);

        // bottom-left
        clipCorner(canvas, mBottomLeftRadius,
                   0, height - mBottomLeftRadius,
                   0, height,
                   mBottomLeftRadius, height,
                   0, height - mBottomLeftRadius * 2,
                   90, 90);
    }

    private void clipCorner(Canvas canvas,
                            float radius,
                            float point1X, float point1Y, float point2X, float point2Y, float point3X, float point3Y,
                            float arcRectTopLeftX, float arcRectTopLeftY,
                            float startAngle, float sweepAngle) {
        if (radius > 0) {
            mPath.reset();
            mPath.moveTo(point1X, point1Y);
            mPath.lineTo(point2X, point2Y);
            mPath.lineTo(point3X, point3Y);
            mOvalRectF.set(arcRectTopLeftX, arcRectTopLeftY,
                           arcRectTopLeftX + radius * 2, arcRectTopLeftY + radius * 2);
            mPath.arcTo(mOvalRectF, startAngle, sweepAngle);
            mPath.close();
            canvas.drawPath(mPath, mClipPaint);
        }
    }

    private void drawStroke(Canvas canvas) {
        if (mStrokeWidth > 0) {
            float narrow = mStrokeWidth / 2f;
            int height = getHeight();
            int width = getWidth();

            mPath.reset();
            mPath.moveTo(mTopLeftRadius + narrow, narrow);
            // top line and top-right arc
            appendLineAndArc(mPath, narrow,
                             width - mTopRightRadius - narrow, narrow, mTopRightRadius,
                             width - mTopRightRadius * 2 + narrow, narrow, 270,
                             mTopRightRadius, 0);
            // right line and bottom-right arc
            appendLineAndArc(mPath, narrow,
                             width - narrow, height - mBottomRightRadius - narrow, mBottomRightRadius,
                             width - mBottomRightRadius * 2 + narrow, height - mBottomRightRadius * 2 + narrow, 0,
                             0, mBottomRightRadius);
            // bottom line and bottom-left arc
            appendLineAndArc(mPath, narrow,
                             mBottomLeftRadius + narrow, height - narrow, mBottomLeftRadius,
                             narrow, height - mBottomLeftRadius * 2 + narrow, 90,
                             -mBottomLeftRadius, 0);
            // left line and top-left arc
            appendLineAndArc(mPath, narrow,
                             narrow, mTopLeftRadius + narrow, mTopLeftRadius,
                             narrow, narrow, 180,
                             0, -mTopLeftRadius);
            mPath.close();
            canvas.drawPath(mPath, mStrokePaint);
        }
    }

    private void appendLineAndArc(Path path, float narrow, float toLineX, float toLineY,
                                  float radius,
                                  float arcRectTopLeftX, float arcRectTopLeftY, float startAngle,
                                  float rLineX, float rLineY) {
        path.lineTo(toLineX, toLineY);
        if (radius > 0) {
            if (narrow < radius) {
                mOvalRectF.set(arcRectTopLeftX, arcRectTopLeftY,
                               arcRectTopLeftX + radius * 2 - narrow * 2, arcRectTopLeftY + radius * 2 - narrow * 2);
                path.arcTo(mOvalRectF, startAngle, 90);
            } else {
                path.rLineTo(rLineX, rLineY);
            }
        }
    }
}

